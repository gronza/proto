﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickController : MonoBehaviour {

    [SerializeField]
    private float minAngle;
    [SerializeField]
    private float maxAngle;

    [SerializeField]
    private Transform targetRotation;

    private float rotateSpeed;

    private float constantRotation;

    private float inputRotateSpeed;

	// Use this for initialization
	void Start () {

        //Initializing all values
        maxAngle = 350f;
        minAngle = 310f;

        targetRotation = transform.parent.GetChild(0);
        targetRotation.localRotation = transform.localRotation;

        rotateSpeed = 5;

        constantRotation = 3f;

        inputRotateSpeed = 200f;
	}
	
	// Update is called once per frame
	void Update () {

        ConstantRotation();
        InputRotation();

        RotateStick();

        //check rotation limits last
        //CheckAngle();
	}

    //Rotates the stick according to player input
    //and game mechanics
    private void RotateStick()
    {
        
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation.localRotation, Time.deltaTime * rotateSpeed);
    }

    //Makes sure that the stick's rotation
    //stays between a maximum and a minimum value
    private bool CheckAngles()
    {
        float z = targetRotation.localRotation.eulerAngles.z;

        //clamps the rotation between two set values
        if ((z <= maxAngle)
            && z >= minAngle)
        {
            return true;
        }
        else
        {
            if (z >= maxAngle)
            {
                targetRotation.Rotate(Vector3.back, 5f);
            }
            else if (z <= minAngle)
            {
                targetRotation.Rotate(Vector3.back, -5f);
            }

            return false;
        }
    }

    //Applies a constant rotation towards
    //the campfire, increases the skill cap of the game
    //and simulates the tiredness of arm muscles
    private void ConstantRotation()
    {
        if (CheckAngles())
        {

            //the effect of constant rotation is greater
            //the closer the marshmallow is to the fire
            float rotationFactor = 360 - targetRotation.localRotation.eulerAngles.z;

            targetRotation.Rotate(Vector3.back, constantRotation * rotationFactor * Time.deltaTime);
        }
    }

    //Rotates the stick according to player input
    private void InputRotation()
    {
        if (CheckAngles())
        {
            targetRotation.Rotate(Vector3.back, -Input.GetAxis("Vertical") * inputRotateSpeed * Time.deltaTime);
        }
    }
}
