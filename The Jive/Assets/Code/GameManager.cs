﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public float score;
    public float tempScore;
    public float timer;

    private Text scoreText;
    private Text tempScoreText;
    private Text timerText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        tempScoreText = GameObject.Find("TempScore").GetComponent<Text>();
        timerText = GameObject.Find("Timer").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();
        tempScoreText.text = tempScore.ToString();
        timerText.text = timer.ToString();
    }
}
