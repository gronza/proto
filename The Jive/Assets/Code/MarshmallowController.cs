﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarshmallowController : MonoBehaviour {

    private Transform marshmallowPosition;
    private GameObject marshmallow;

    [SerializeField]
    private GameObject marshmallowPrefab;

    private float growSpeed;
    private bool canDrop;
    private float dropAngle;

	// Use this for initialization
	void Start () {

        marshmallowPosition = transform.GetChild(0).transform;
        dropAngle = 345f;

        SpawnMarshmallow();

	}
	
	// Update is called once per frame
	void Update () {
        DropMarshmallow();

        if(transform.localRotation.eulerAngles.z < dropAngle)
        {
            canDrop = true;
        }
	}

    //Spawns a new marshmallow on the stick
    private void SpawnMarshmallow()
    {
        marshmallow = Instantiate(marshmallowPrefab,
            marshmallowPosition.position,
            marshmallowPosition.rotation,
            marshmallowPosition);

        canDrop = false;
    }

    private void DropMarshmallow()
    {
        if(transform.localRotation.eulerAngles.z >= dropAngle
            && Input.GetAxis("Vertical") >= 1
            && canDrop)
        {
            marshmallow.GetComponent<Roast>().Drop();
            SpawnMarshmallow();
        }
    }
    
}
