﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roast : MonoBehaviour
{
    private Renderer rend;
    private GameManager manager;

    private float score;
    private float maxScore;
    private float punishFactor;

    //Offset applied to texture, should be between 0 and 0.8
    [SerializeField]
    private float roast;

    private float roastSpeed;
    private float maxRoast;
    private float perfectRoast;

    private Vector3 origScale;
    private bool growing;
    private float growTime;
    private bool shrinking;

    private bool dropped;
    private float droppedSpeed;
    private float droppedRotation;
    private Vector3 droppedTarget;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();

        maxScore = 1000f;
        punishFactor = 2000f;

        roast = -0.1f;
        roastSpeed = 0.05f;
        maxRoast = 0.8f;
        perfectRoast = 0.4f;

        origScale = transform.localScale;
        transform.localScale = new Vector3(0, 0);
        growing = true;
        growTime = 0.5f;
        shrinking = false;

        dropped = false;
        droppedSpeed = 2;
        droppedRotation = -90;
        droppedTarget = GameObject.Find("DroppedTarget").transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float offset = roast;
        rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));

        if (growing)
        {
            Grow();
        }
        if (shrinking)
        {
            Shrink();
        }

        if (dropped)
        {
            transform.Translate(( droppedTarget - transform.position) * droppedSpeed * Time.deltaTime);
            transform.Rotate(Vector3.back, droppedRotation * Time.deltaTime);
        }
    }

    //The marshmallow get roasted depending on how close it is to the fire
    private void OnTriggerStay2D(Collider2D collision)
    {

        if (roast < maxRoast)
        {
            switch (collision.tag)
            {
                case "Fire4":
                    roast += roastSpeed * Time.deltaTime * 50;
                    break;

                case "Fire3":
                    roast += roastSpeed * Time.deltaTime * 20;
                    break;

                case "Fire2":
                    roast += roastSpeed * Time.deltaTime * 10;
                    break;

                case "Fire1":
                    roast += roastSpeed * Time.deltaTime * 5;
                    break;
            }
        }
    }

    //The marshmallow will grow to it's scale from
    //0 upon spawning
    private void Grow()
    {
        transform.localScale = new Vector3(transform.localScale.x + origScale.x / growTime * Time.deltaTime,
            transform.localScale.y + origScale.y / growTime * Time.deltaTime);

        if(transform.localScale.x >= origScale.x)
        {
            growing = false;
        }
    }

    //The marshmallow will shrink to 0 upon being dropped
    private void Shrink()
    {
        transform.localScale = new Vector3(transform.localScale.x - origScale.x / growTime * Time.deltaTime,
            transform.localScale.y - origScale.y / growTime * Time.deltaTime);
        
        if (transform.localScale.x < 0)
        {
            shrinking = false;
            transform.localScale = new Vector3(0, 0);
        }
        
    }

    //The marshmallow flies off the stick and it's score value is added to the round score
    public void Drop()
    {
        transform.parent = null;
        dropped = true;
        shrinking = true;
        Score();
        Destroy(this.gameObject, 1f);
    }

    //Apply a score value to the marshmallow when it's dropped
    private void Score()
    {
        score = Mathf.RoundToInt(maxScore - (Mathf.Abs(perfectRoast - roast) * punishFactor));
        manager.tempScore = score;
    }
}
